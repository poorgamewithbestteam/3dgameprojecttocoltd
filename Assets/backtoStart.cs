using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class backtoStart : MonoBehaviour
{
    public int startSceneIndex;
    void Start()
    {
        startSceneIndex = SceneManager.GetActiveScene().buildIndex - 5;
    }

   public void gotoStart()
    {
        SceneManager.LoadScene(startSceneIndex);
    }
}
