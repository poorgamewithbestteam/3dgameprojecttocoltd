using UnityEngine;
using UnityEngine.SceneManagement;

public class backto1sr : MonoBehaviour
{
    public int StartIndex;
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None; // Lock the cursor
        Cursor.visible = true;
        StartIndex = SceneManager.GetActiveScene().buildIndex - 3;
    }
    public void StartScene()
    {
        SceneManager.LoadScene(StartIndex);
        Cursor.lockState = CursorLockMode.Locked; // Lock the cursor
        Cursor.visible = false;
    }
}
