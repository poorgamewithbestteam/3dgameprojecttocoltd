using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class creditScene : MonoBehaviour
{
    public int CreditIndex;
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None; // Release the cursor
        Cursor.visible = true;
        CreditIndex = SceneManager.GetActiveScene().buildIndex + 2;
    }
    public void PassScene()
    {
        SceneManager.LoadScene(CreditIndex);
        
    }
}

