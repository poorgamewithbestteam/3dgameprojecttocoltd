using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ItemCheckBar : MonoBehaviour
{
    public Image[] keyItemImages; // An array of key item images
    public int nextSceneIndex; // Name of the next scene to load

    public int collectedKeyItems = 0;

    private void Start()
    {
        // Hide all key item images initially
        foreach (Image keyItemImage in keyItemImages)
        {
            nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
            keyItemImage.gameObject.SetActive(false);
        }
    }

    public void CollectKeyItem()
    {
        if (collectedKeyItems < keyItemImages.Length)
        {
            // Activate the next key item image
            keyItemImages[collectedKeyItems].gameObject.SetActive(true);
            collectedKeyItems++;

            // Check if all key items have been collected
            if (collectedKeyItems == keyItemImages.Length)
            {
                // All key items have been collected, go to the next scene
                LoadNextScene();
            }
        }
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene(nextSceneIndex);
    }
}
