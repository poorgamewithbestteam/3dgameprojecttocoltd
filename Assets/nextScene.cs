using UnityEngine;
using UnityEngine.SceneManagement;

public class nextScene : MonoBehaviour
{
    public int sceneIndex;
    private void Start()
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
    }
    public void NextScene()
    {
        SceneManager.LoadScene(sceneIndex);
        Cursor.lockState = CursorLockMode.Locked; // Lock the cursor
        Cursor.visible = false;
    }
}
